author: Nicholas Young-Soares
name: Marvin 2k
description:  CSS, XHTML for all layout. (No tables for layout). 

This theme is a port of the original Marvin_2k theme to PHPtemplate (for Drupal 4.5).
It should look like as close to the original Marvin_2k as possible.