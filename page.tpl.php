<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $custom_css ?>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"> </script>
</head>

<body<?php print $onload_attributes ?>>
<div class="pageFrame" id="nodeFrame">
  <div id="header">
    <?php if ($logo) : ?>
    <a href="<?php print url() ?>" title="Index Page"><img src="<?php print($logo) ?>" alt="Logo" /></a>
    <?php endif; ?>
    
    <?php if ($site_name) : ?>
      <a href="<?php print url() ?>" title="Index Page"><h1 id="site-name"><?php print($site_name) ?></h1></a>
    <?php endif;?>
    <?php if ($site_slogan) : ?>
      <span id="site-slogan"><?php print($site_slogan) ?></span>
    <?php endif;?>
    
     <?php if ($search_box): ?>
        <form action="<?php print url("search") ?>" method="post">
	  <div id="search">
	    <input class="form-text" type="text" size="15" value="" name="edit[keys]" /><input class="form-submit" type="submit" value="<?php print(t("Search"))?>" />
	  </div>
	</form>
     <?php endif; ?>
  </div>

  <?php if (is_array($primary_links)): ?>
    <ul id="main-nav">
      <?php foreach ($primary_links as $link): ?>
         <li><?php print $link?></li>
      <?php endforeach; ?>
    </ul>
  <?php endif; ?>

<?php if ($sidebar_left != ""): ?>
  <div class="sidebar" id="sidebar-left">
    <?php print $sidebar_left ?>
  </div>
<?php endif; ?>

<?php if ($sidebar_right != ""): ?>
  <div class="sidebar" id="sidebar-right">
    <?php print $sidebar_right ?>
  </div>
<?php endif; ?>
  
  <div class="main-content" id="content-<?php print $layout ?>">
<?php if (trim(strip_tags($breadcrumb))) { print $breadcrumb; } ?>
<?php if ($mission != ""): ?>
  <p id="mission"><?php print $mission ?></p>
<?php endif; ?>
<?php if ($title != ""): ?>
  <h1 class="title"><?php print $title ?></h1>
<?php endif; ?>
<?php if ($tabs != ""): ?>
  <div class="tabs"><?php print $tabs ?></div>
<?php endif; ?>
<?php if ($help != ""): ?>
  <p id="help"><?php print $help ?></p>
<?php endif; ?>
<?php if ($messages != ""): ?>
  <div id="message"><?php print $messages ?></div>
<?php endif; ?>

  <!-- start main content -->
  <?php print($content) ?>
  <!-- end main content -->

  <div id="footer">
   <?php if ($footer_message) : ?>
   <p><?php print $footer_message;?></p>
  <?php endif; ?>
  </div><!-- footer -->
 </div><!-- mainContent -->
</div><!-- pageFrame -->
 <?php print $closure;?>
</body>
</html>

